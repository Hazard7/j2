package filesystem;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class File {
    private String name;
    private Directory parentDirectory;
    protected Lock mutex;

    public File() {}

    public File(String name) {
        this.name = name;
        this.parentDirectory = null;
        this.mutex = new ReentrantLock();
    }

    public String getName() {
        return name;
    }

    public Directory getParentDirectory() {
        return parentDirectory;
    }

    public void setParentDirectory(Directory directory) {
        try {
            mutex.lock();
            this.parentDirectory = directory;
        } finally {
            mutex.unlock();
        }
    }

    public void move(Directory newDirectory) {
        try {
            mutex.lock();
            parentDirectory.deleteFile(this);
            newDirectory.insertFile(this);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            mutex.unlock();
        }
    }

    public void delete() {
        try {
            mutex.lock();
            if (parentDirectory != null) {
                parentDirectory.deleteFile(this);
            }
        } finally {
            mutex.unlock();
        }
    }

    public String getDescription() {
        return getName() + " (type: " + getClass().getSimpleName() + ")";
    }

    public String getPath() {
        if (parentDirectory == null) return "/" + getName();
        return ((File)parentDirectory).getPath() + "/" + getName();
    }

    public long count(boolean recursive) {
        return new ForkJoinPool().invoke(new FileCounter(this, recursive));
    }

    public long depth() {
        return new ForkJoinPool().invoke(new FileDepthFinder(this));        
    }

    public List<String> search(String pattern) {
        return new ForkJoinPool().invoke(new FileSearcher(this, pattern));
    }

    public String tree() {
        JSONStringer json = new ForkJoinPool().invoke(new FileTreeJsonBuilder(this));
        return json.toString();
    }
}
